## this is how u use the methods (note that the 'hashFc' shall be overriten) ##

<br>hashSize = 17 # size of the hash table (indexes 0-16)
<br>tombNum = 0 # here put number witch you never insert
<br>hashMap = {} # just let it here for init
<br>separ = "," # separator of cells in printing the hash table 
<br>numOfSpaces = 4 # num of spaces of a single call in printing the hash table 
<br>
<br>in the "def hashFc(k, i)" you define the hash function for key 'k' on iteration 'i' (starting from 0)
<br>
<br>insertToHash(hashMap, [19, 69, 53, 52, 27, 18]) # inserts the numbers from left to right, using the open addressing method with 'hashFc(k, i)'    hash function; can overrite the tombstones
<br>printHash(hashMap) # just prints out with the "print()" fc
<br>hashMap = deleteFromHash(hashMap, [27, 69]) # removes the numbers from left to right and substitues them with "tombstones" (exactly, by the 'tombNum')
