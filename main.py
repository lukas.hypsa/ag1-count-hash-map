def hashFc(k, i):
    x = (((6 * k) + (i * (((4 * k) % 7) + 1))) % 17)
    return x

def printHash(hashMap):
    # print keys
    for ind in list(range(0, hashSize)):
        p = ind
        print('{:4d}'.format(p) + separ, end="")
    print()
    # print values
    for ind in list(range(0, hashSize)):
        p = 0
        if ind in hashMap:
            if hashMap[ind] == tombNum:
                print("   t" + separ, end="")
            else:
                p = hashMap[ind]
                print('{:4d}'.format(p) + separ, end="")
        else:
            print('   -'.format(p) + separ, end="")
    print()

def insertToHash(hashMap, toInsert):
    # insert keys
    for k in toInsert:
        wasTomb = False
        i = 0
        x = hashFc(k, i)
        while x in hashMap:
            if(hashMap[x] == tombNum):
                wasTomb = True
                break
            i = (i + 1)
            x = hashFc(k, i)
        hashMap[x] = k
        print("ins " + str(k) + " -> " + str(x), end="")
        if(wasTomb):
            print(" (over tomb)")
        print()

def deleteFromHash(hashMap, toDelete):
    for k in toDelete:
        for ind in hashMap:
            if ind in hashMap:
                if (hashMap[ind] == k):
                    hashMap[ind] = 0
                    print("remo " + str(k))
    return hashMap

hashSize = 17
tombNum = 0
hashMap = {}
separ = ", "
numOfSpaces = 4

insertToHash(hashMap, [19, 69, 53, 52, 27, 18])
printHash(hashMap)
hashMap = deleteFromHash(hashMap, [27, 69])
printHash(hashMap)
insertToHash(hashMap, [29, 37, 18])
printHash(hashMap)